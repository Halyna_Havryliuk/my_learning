﻿using System;
using System.Collections.Generic;

namespace Proxy
{
    internal partial class SpaceBookSystem //needed to make Spacebook a private class, it's not an element of pattern
    {
        private class SpaceBook
        {
            static Dictionary<string, SpaceBook> community = new Dictionary<string, SpaceBook>();
            string pages = "";
            string name = null;
            string gap = "\n\t\t\t\t";

            public SpaceBook(string name)
            {
                this.name = name;
                community[name] = this;
            }

            public void Add(string message) //add message to your page
            {
                pages += gap + message;
                Console.Write(gap + "======== " + name + "'s SpaceBook =========");
                Console.Write(pages);
                Console.WriteLine(gap + "===================================");
            }

            public void Add(string to, string message) //add message to your friend's page
            {
                community[to].Add(message);
            }

            public void Poke(string to, string from) //special message for friend - poke
            {
                community[to].pages += gap + from + " poked you";
            }

            internal static bool NameCanBeAdded(string name)
            {
                return !string.IsNullOrWhiteSpace(name) && !community.ContainsKey(name);
            }
        }
    }
}