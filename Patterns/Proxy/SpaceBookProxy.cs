﻿using System;

namespace Proxy
{
    partial class SpaceBookSystem
    {
        public class SpaceBookProxy //Proxy for creation actual SpaceBook if needed and for handling authentification
        {
            SpaceBook spaceBook = null;
            bool loggedIn = false;
            string name = null;
            string password = null;

            public void Add(string message)
            {
                Check();

                if (loggedIn)
                {
                    spaceBook.Add(message);
                }
            }

            public void Add(string to, string message)
            {
                Check();

                if (loggedIn)
                {
                    spaceBook.Add(to, name + " said: " + message);
                }
            }

            public void Poke(string to)
            {
                Check();
                if (loggedIn)
                {
                    spaceBook.Poke(to, name);
                }
            }

            private void Check()
            {
                if (!loggedIn)
                {
                    if (password == null)
                    {
                        Register();
                    }

                    if (spaceBook == null)
                    {
                        Authenticate();
                    }
                }
            }

            private void Authenticate()
            {
                Console.Write("Welcome " + name + ". Please type in your password: ");
                string supplied = Console.ReadLine();
                if (supplied == password)
                {
                    loggedIn = true;
                    Console.WriteLine("Logged into SpaceBook");
                    if (spaceBook == null)
                    {
                        spaceBook = new SpaceBook(name);
                    }
                }
                else
                {
                    Console.WriteLine("Incorrect password");
                }
            }

            private void Register()
            {
                Console.WriteLine("Let's register you for SpaceBook");

                do
                {
                    Console.WriteLine("All SpaceBook names must be unique");
                    Console.Write("Type in a user name: ");
                    name = Console.ReadLine();
                }
                while (!SpaceBook.NameCanBeAdded(name));

                Console.Write("Type in a password: ");
                password = Console.ReadLine();

                Console.WriteLine("Thanks for registering!");
            }
        }
    }
}
