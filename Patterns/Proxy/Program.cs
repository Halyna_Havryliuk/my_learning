﻿namespace Proxy
{
    class Program : SpaceBookSystem //client
    {
        static void Main(string[] args)
        {
            SpaceBookProxy me = new SpaceBookProxy();
            me.Add("Hello World!");
            me.Add("Today I worked 18 hours");

            SpaceBookProxy tom = new SpaceBookProxy();
            tom.Poke("Judith");
            tom.Add("Judith", "Poor you");
            tom.Add("Off to see the Lion King tonight");
        }
    }
}
