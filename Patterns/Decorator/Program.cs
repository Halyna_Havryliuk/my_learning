﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Decorator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Photo photo = new Photo();
            Application.Run(photo);

            TaggedPhoto foodTagggedPhoto = new TaggedPhoto(photo, "Food");
            TaggedPhoto colorTaggedPhoto = new TaggedPhoto(foodTagggedPhoto, "Yellow");
            BorderedPhoto composition = new BorderedPhoto(colorTaggedPhoto, Color.Blue);

            Application.Run(composition);

            MessageBox.Show(colorTaggedPhoto.GetTags());

            TaggedPhoto lemonadeTaggedPhoto = new TaggedPhoto(photo, "Lemonade");
            composition = new BorderedPhoto(lemonadeTaggedPhoto, Color.Yellow);

            Application.Run(composition);

            MessageBox.Show(lemonadeTaggedPhoto.GetTags());
        }
    }
}
