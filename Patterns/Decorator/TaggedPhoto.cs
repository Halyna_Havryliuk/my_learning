﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Decorator
{
    internal class TaggedPhoto : Photo
    {
        private Photo photo;
        private string tag;

        int number;
        static int count;

        static List<string> tags = new List<string>();

        public TaggedPhoto(Photo photo, string tag)
        {
            this.photo = photo;
            this.tag = tag;
            tags.Add(tag);
            number = ++count;
        }

        public override void OnPaint(object sender, PaintEventArgs e)
        {
            photo.OnPaint(sender, e);
            e.Graphics.DrawString(tag, new Font("Arial", 16), new SolidBrush(Color.Black), 80, 100 + number * 20);
        }
        
        internal string GetTags()
        {
            string res = "Tags: ";
            foreach (var tag in tags)
            {
                res += tag + ", ";
            }

            return res;
        }
    }
}