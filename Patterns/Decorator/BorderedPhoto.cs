﻿using System.Drawing;
using System.Windows.Forms;

namespace Decorator
{
    internal class BorderedPhoto : Photo
    {
        private Photo photo;
        private Color color;

        public BorderedPhoto(Photo p, Color c)
        {
            photo = p;
            color = c;
        }

        public override void OnPaint(object sender, PaintEventArgs e)
        {
            photo.OnPaint(sender, e);
            e.Graphics.DrawRectangle(new Pen(color, 10), 0, 0, photo.Width - 10, photo.Height - 10);
        }
    }
}