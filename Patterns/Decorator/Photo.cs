﻿using System.Drawing;
using System.Windows.Forms;

namespace Decorator
{
    internal class Photo : Form
    {
        Image image = new Bitmap("../../Images/lemonade.jpg");

        public Photo()
        {
            Size = image.Size;
            Text = "Lemonade";
            Paint += OnPaint;
        }

        public virtual void OnPaint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(image, 0, 0);
        }
    }
}